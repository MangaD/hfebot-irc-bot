@echo off

set exeName=HFEBot

set curDir="%CD%"
set srcDir="%CD%\src"
set targetDir="%CD%\bin"
set docDir="%CD%\doc"

IF [%1] == [] (
	echo Use with parameter "compile", "doc" or "run".
	exit /b
)
IF %1 == compile goto compile
IF %1 == doc goto doc
IF %1 == run goto run

:compile
cd %srcDir%
javac -encoding "UTF-8" botclient\*.java
jar cfe %exeName%.jar botclient.BotClient botclient\*.class
del botclient\*.class
mv %exeName%.jar %targetDir%
cd %curDir%
exit /b

:doc
javadoc -d %docDir% %srcDir%\botclient\*.java
exit /b

:run
cd %targetDir%
start java -jar %exeName%.jar
cd %curDir%
exit /b
