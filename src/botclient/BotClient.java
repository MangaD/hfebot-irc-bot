/**
 * BotClient class, contains the initialization steps of the program.
 * Reads and parses a XML file that contains the data defined by the
 * user for starting the program.
 * @author MangaD
 */

package botclient;

//IO Imports
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.UnknownHostException;
//Util Imports
import java.util.List;
import java.util.ArrayList;
//XML Imports
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class BotClient {
	
	public static List<IRCConnection> servers = new ArrayList<IRCConnection>();
	
	private static String version = "HFEBot 25042015";
	private static String nick = "HFEBot";
	private static String ident = "HFEBot";
	private static String realname = "HFEBot";
	private static Document init_doc;
	
	public static void main(String args[]) throws IOException {
		
		BufferedReader cmd = new BufferedReader(new InputStreamReader(System.in));
		
		try{
			parseXmlFile("init.xml");
		} catch(ParserConfigurationException|SAXException|IOException e) {
			System.out.println("Error parsing init.xml: " + e.getMessage());
			System.exit(0);
		}
		
		init();
		
		cmd.readLine();
		
		terminate();
		cmd.readLine();
	
	}
	
	public static String getVersion(){
		return version;
	}
	public static String getNick(){
		return nick;
	}
	public static String getIdent(){
		return ident;
	}
	public static String getRealname(){
		return realname;
	}
	
	public static void onJoin(IRCConnection con, String channel){
		con.msg(channel, "I play ping pong!" );
		con.me(channel, "plays ping pong." );
	}
	
	private static void parseXmlFile(String uri) 
		throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		init_doc = db.parse(uri);	
	}
	
	private static void init(){
		Element rootEle = init_doc.getDocumentElement();
		//Get nick, ident and realname, if specified
		//must verify if they are valid
		String ele = getTextValue(rootEle,"nick");
		if(ele != null)
			nick = ele;
		ele = getTextValue(rootEle,"ident"); 
		if(ele != null)
			ident = ele;
		ele = getTextValue(rootEle,"realname");
		if(ele != null)
			realname = ele;
		//Get list of servers to connect to
		NodeList nl = rootEle.getElementsByTagName("server");
		if(nl != null && nl.getLength() > 0) {
			for(int i = 0 ; i < nl.getLength();i++) {
				Element el = (Element)nl.item(i);
				boolean start = Boolean.parseBoolean(getTextValue(el,"start"));
				if(!start) 
					continue;
				IRCConnection con = getIRCConnection(el);
				if(con != null)
					servers.add(con);
			}
		}
	}
	
	private static void terminate(){
		for(IRCConnection con : new ArrayList<IRCConnection>(servers)){
			for(String channel : new ArrayList<String>(con.channels)){
				System.out.println("PARTING CHANNEL: " + channel);
				con.part(channel, "See you!");
			}
			System.out.println("QUITING SERVER: " + con.getHostName());
			con.quit("Leaving.");
		}
	}
	
	private static IRCConnection getIRCConnection(Element el) {
		String name = getTextValue(el,"name");
		if(name == null) return null;
		int port;
		try{
			port = getIntValue(el,"port");//Must check if valid
			if(port < 1 || port > 65535) {
				System.out.println("Port number for server " + name + " must be between 1 and 65535.");
				return null;
			}
		} catch (NumberFormatException nfe) {
			System.out.println("Port for server " + name + " must be a number between 1 and 65535.");
			return null;
		}
		IRCConnection con = null;
		NodeList nl = el.getElementsByTagName("identify");
		String service = null;
		String message = null;
		if(nl != null && nl.getLength() > 0){
			Element ele = (Element)nl.item(0);
			service = getTextValue(ele,"service");
			message = getTextValue(ele,"message");
		}
		if(service != null && message != null){
			try{
				con = new IRCConnection( name, port, nick, ident, 
					realname, service, message );
			}
			catch (UnknownHostException uhe) {
				System.out.println("Host " + name + " not found.");
				return null;
			}
			catch (IOException e){ return null; }
		}
		else{
			try{
				con = new IRCConnection( name, port, nick, ident, realname );
			}
			catch (UnknownHostException uhe) {
				System.out.println("Host " + name + " not found.");
				return null;
			}
			catch (IOException e){ return null; }
		}
		nl = el.getElementsByTagName("channelList");
		Element ele = (Element)nl.item(0);
		nl = ele.getElementsByTagName("channel");
		if(nl != null && nl.getLength() > 0) {
			for(int i = 0 ; i < nl.getLength();i++) {
				Element ch = (Element)nl.item(i);
				String channel = ch.getFirstChild().getNodeValue();
				con.join( channel );
				onJoin(con, channel);
			}
		}
		return con;
	}
	
	private static String getTextValue(Element el, String tagName)
	{
		String textVal = null;
		NodeList nl = el.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element ele = (Element)nl.item(0);
			textVal = ele.getFirstChild().getNodeValue();
		}
		return textVal;
	}
	
	private static int getIntValue(Element el, String tagName) 
		throws NumberFormatException 
	{
		return Integer.parseInt(getTextValue(el,tagName));
	}
}
