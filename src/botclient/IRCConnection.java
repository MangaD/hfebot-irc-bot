/*  IRCConnection Class *
 *  @author MangaD   */
 
package botclient;

//IO Imports
import java.io.PrintStream;
import java.io.IOException;
import java.io.InputStream;
//Net Imports
import java.net.UnknownHostException;
import java.net.Socket;
import java.net.InetAddress;
//Util Imports
import java.util.List;
import java.util.ArrayList;

public class IRCConnection {
	
	public String nick;
	public List<String> channels = new ArrayList<String>();
	
	private String service = null; //For authing to account
	private String host_name;
	private String host_ip;
	private int port;
	private String ident;
	private String realname;
	private PrintStream out;
	private InputStream in;
	private InputParser t;
	private volatile boolean status = false;
	
	public IRCConnection( String host, int port , String nick, String ident, String realname) throws UnknownHostException, IOException {
		this.host_name = host;
		this.port = port;
		this.nick = nick;
		this.ident = ident;
		this.realname = realname;
		connect();
		register();
	}
	
	public IRCConnection( String host, int port , String nick, String ident, String realname, String service, String credentials) throws UnknownHostException, IOException {
		this.host_name = host;
		this.port = port;
		this.nick = nick;
		this.ident = ident;
		this.realname = realname;
		this.service = service;
		connect();
		register();
		authenticate(this.service, credentials);
	}
	
	public String getHostName(){
		return this.host_name;
	}
	public String getServiceName(){
		return this.service;
	}
	
	private void connect() throws UnknownHostException, IOException {
		
		System.out.println( "Looking up " + host_name );
		InetAddress address = InetAddress.getByName(this.host_name);
		this.host_ip = address.getHostAddress();
		System.out.println("Connecting to " + host_name + " (" + this.host_ip + ":" + this.port + ")" );
		Socket socket = new Socket( host_ip, port );
		
		out = new PrintStream( socket.getOutputStream() );
		in = socket.getInputStream();
		System.out.print("Connected. ");
		
		t = new InputParser( this, in , out );
		t.setDaemon( true );
		t.start();
	}
	
	public void register(){
		System.out.println("Now logging in.");
		out.println( "NICK " + this.nick);
		
		//USER username hostname servername :realname
		//out.println( "USER " + this.ident + " localhost " + this.host_name + " :" + this.realname);
		//or
		//USER username 8 * :realname
		out.println( "USER " + this.ident + " 8 * :" + this.realname);
		
		//Wait until it gets registered, so it can join channels.
		while(!t.isRegistered());
	}
	
	public void authenticate(String service, String message){
		this.service = service;
		System.out.println("Authing to your account.");
		this.msg(service, message);
		while(!t.isAuthenticated());
	}
	
	public void join( String channel ){
		System.out.println("Joining channel " + channel );
		out.println( "JOIN " + channel );
	}
	
	public void part( String channel, String msg ){
		out.println ( "PART " + channel + " :" + msg );
		this.channels.remove(channel);
	}
	
	public void quit(String msg){
		out.println( "QUIT :" + msg);
		BotClient.servers.remove(this);
	}
	
	public void nick(String nick){
		out.println( "NICK :" + nick);
	}
	
	public void notice( String user, String msg )
	{
		out.println ( "NOTICE "  + user + " :" + msg );
	}
	
	public void msg( String receiver, String msg )
	{
		out.println ( "PRIVMSG "  + receiver + " :" + msg );
	}
	
	public void away( String msg )
	{
		out.println ( "AWAY :" + msg );
		this.status = true;
	}
	
	public void back()
	{
		out.println ( "AWAY" );
		this.status = false;
	}
	
	public boolean isAway(){
		return this.status;
	}
	
	//CTCP ACTION
	public void me(String receiver, String msg){
		out.println ( "PRIVMSG "  + receiver + " :\001ACTION " + msg +"\001" );
	}
	//CTCP TIME
	public void time(String receiver){
		out.println ( "PRIVMSG "  + receiver + " :\001TIME\001" );
	}
	
	public void mode( String channel, String mode, String user){
		out.println ( "MODE "  + channel + " " + mode + " " + user );
	}
	
	public void kick(String channel, String user, String msg){
		out.println ( "KICK "  + channel + " " + user + " :" + msg );
	}
	
}