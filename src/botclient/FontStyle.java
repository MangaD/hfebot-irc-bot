/*  Font Style Class *
 *  @author MangaD   */
 
package botclient;

//Static class
public final class FontStyle {
	
	private FontStyle(){}
	
	//Ctrl+B
	public static String bold(String msg){
		return "\002" + msg + "\002";
	}
	//Ctrl+R
	public static String reversed_italic(String msg){
		return "\026" + msg + "\026";
	}
	//Ctrl+U
	public static String underline(String msg){
		return "\037" + msg + "\037";
	}
/* Ctrl+K
 * 0-   White 
 * 1-   Black 
 * 2-   Blue 
 * 3-   Green 
 * 4-   Light Red 
 * 5-   Brown 
 * 6-   Purple 
 * 7-   Orange 
 * 8-   Yellow 
 * 9-   Light Green 
 * 10-   Cyan 
 * 11-   Light Cyan 
 * 12-   Light Blue 
 * 13-   Pink 
 * 14-   Grey 
 * 15-   Light Grey 
 */
	public static String color(String msg, String foreground){
		return "\003" + foreground + msg + "\003";
	}
	public static String color(String msg, String foreground, String background){
		return "\003" + foreground + "," +background + msg + "\003";
	}
}