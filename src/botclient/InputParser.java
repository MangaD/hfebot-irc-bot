/*  InputParser Class *
 *  @author MangaD   */
 
package botclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintStream;

public class InputParser extends Thread
{
	private IRCConnection con;
	private BufferedReader in;
	private PrintStream out;
	private volatile boolean registered = false;
	private volatile boolean authenticated = false;
	private char prefixChar = '.';
	
	protected InputParser( IRCConnection con , InputStream in , PrintStream out){
		this.con = con;
		this.in = new BufferedReader(new InputStreamReader(in));
		this.out = out;
	}
	
	public void run(){
		try{
			String msg;
			//While registering
			while ( ( msg = in.readLine() ) != null  && !this.isRegistered()){
				if(msg.substring(0,4).equals("PING")){
					String pong = "PONG" + msg.substring(4);
					out.println(pong);
				}
				else {
					System.out.println( msg );
					if (msg.contains("Nickname is already in use.")){
						char nickSuffix = '_';
						System.out.println( "The nickname \"" + this.con.nick +
						"\" is already in use, trying \"" + this.con.nick + nickSuffix + "\"" );
						this.con.nick = this.con.nick + nickSuffix;
						this.con.nick(this.con.nick);
					}
					else if(!msg.contains("NOTICE"))
						this.registered = true;
				}
			}
			//If authing right after registering
			if(con.getServiceName() != null){
				while ( ( msg = in.readLine() ) != null  && !this.isAuthenticated()){
					if(msg.substring(0,4).equals("PING")){
						String pong = "PONG" + msg.substring(4);
						out.println(pong);
					}
					else {
						System.out.println( msg );
						if(msg.contains(this.con.getServiceName()))
							this.authenticated = true;
					}
				}
			}
			//After registering (and authing)
			while ( ( msg = in.readLine() ) != null ){
				if(msg.substring(0,4).equals("PING")){
					String pong = "PONG" + msg.substring(4);
					out.println(pong);
				}
				else{
					System.out.println( msg );
					commands(msg);
				}
			}
		}
		catch( IOException e ){
			e.printStackTrace();
		}
	}

	
	//Check if bot is registered in the server
	public boolean isRegistered(){
		return this.registered;
	}
	
	//Check if bot is authenticated in the server
	public boolean isAuthenticated(){
		return this.authenticated;
	}
	
	//Get someone's username
	public String getUsername(String msg){
		try{
			return msg.substring(1,msg.indexOf("!"));
		} catch (Exception e) { return ""; }
	}
	
	//Get someone's ident
	public String getIdent(String msg){
		try{
			return msg.substring(msg.indexOf("!")+1, msg.indexOf("@"));
		} catch (Exception e) { return ""; }
	}
	
	//Returns the content of a private message
	public String getPrivmsg(String msg){
		if(msg.indexOf("PRIVMSG") < 0)
			return null;
		try{
			String afterPrivmsg = msg.substring( msg.indexOf("PRIVMSG") );
			return afterPrivmsg.substring( afterPrivmsg.indexOf(" :")+2 );
		} catch (Exception e) { return null; }
	}
	
	//If someone changes nickname, this function returns the new nick
	public String getNick(String msg){
		int indexOfNick = msg.indexOf("NICK");
		//PRIVMSG is a valid nickname, make sure that NICK is not a part of a private message.
		int indexOfPrivmsg = msg.indexOf("PRIVMSG");
		if(indexOfNick < 0 || (indexOfPrivmsg > 0 && indexOfPrivmsg < indexOfNick))
			return null;
		try{
			String afterNick = msg.substring( msg.indexOf("NICK") );
			return afterNick.substring( afterNick.indexOf(" :")+2 );
		} catch (Exception e) { return null; }
	}
	
	//Gets the receiver of the message, either a channel or this bot.
	public String getReceiver(String msg){
		if(msg.indexOf("PRIVMSG") < 0)
			return null;
		try{
			String afterPrivmsg = msg.substring( msg.indexOf("PRIVMSG ")+8 );
			return afterPrivmsg.substring( 0, afterPrivmsg.indexOf(" :") );
		} catch (Exception e) { return null; }
	}
	
	public boolean isChannel(String name){
		if(name.charAt(0) == '#' || name.charAt(0) == '&') return true;
		return false;
	}
	
	public void commands(String msg){
		//Check if it is a private message
		String afterPrivmsg = getPrivmsg(msg);
		if(afterPrivmsg == null){
			//check if Bot changed nick
			String newNick = getNick(msg);
			if(newNick != null && getUsername(msg).equals(this.con.nick)){
				this.con.nick = newNick;
			}
			//check if Bot joined channel
			else if(msg.contains(" 366 " + this.con.nick )){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals("366")) break;
				if(tokens.length > ++i){
					if(isChannel(tokens[++i]))
						this.con.channels.add(tokens[i]);
				}
			}
			//check if Bot left channel
			else if( msg.contains(" KICK ") ){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals("KICK")) break;
				if(tokens.length > ++i){
					if(isChannel(tokens[i]) && tokens[i+1].equals(this.con.nick))
						this.con.channels.remove(tokens[i]);
				}
			}
			//Not a private message, so return.
			return;
		}
		//CTCP replies
		if(afterPrivmsg.startsWith("\001")){
			if(afterPrivmsg.startsWith("\001TIME\001")){
				java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
				java.util.Date date = new java.util.Date();
				this.con.notice( getUsername(msg), "\001TIME " + dateFormat.format(date) + "\001");
			}
			else if(afterPrivmsg.startsWith("\001VERSION\001")){
				String OS = System.getProperty("os.name");
				String arch = System.getProperty("os.arch");
				this.con.notice( getUsername(msg), "\001VERSION " + BotClient.getVersion() + 
				" [" + arch + "] / " + OS + "\001");
			}
			else if(afterPrivmsg.startsWith("\001PING")){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals(":\001PING")) break;
				if(++i < tokens.length && !tokens[i].contains("\001")) return;
				this.con.notice( getUsername(msg), "\001PING " + (tokens.length > i ? tokens[i] : "\001"));
			}
			return;
		}
		
		//Check where the reply is going to, either a channel or a specific user
		String receiver = getReceiver(msg);
		if(receiver == null) return;
		if( !isChannel(receiver) ){
			receiver = getUsername(msg);
		}
		//Toys
		if(afterPrivmsg.toLowerCase().startsWith(this.con.nick.toLowerCase() + "!") && !this.con.isAway()){
			this.con.msg(receiver, getUsername(msg) + "!");
		}
		//Commands starting with prefix character
		else if( afterPrivmsg.charAt(0) == prefixChar ){
			afterPrivmsg = afterPrivmsg.substring(1);
			if(afterPrivmsg.startsWith("ping") && !this.con.isAway()){
				this.con.msg(receiver, FontStyle.bold( getUsername(msg) ) + ": Pong!");
			}
			//Commands
			else if(afterPrivmsg.startsWith("time") && !this.con.isAway()){
				java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("EEEE, dd/MM/yyyy G hh:mm:ss a, zzzz (XXX)");
				dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("Europe/London"));
				java.util.Date date = new java.util.Date();
				this.con.msg(receiver, dateFormat.format(date));
			}
			//Admin commands
			else if(afterPrivmsg.startsWith("kick") && !this.con.isAway()){
				if(!isChannel(receiver)){
					this.con.notice(getUsername(msg), getReceiver(msg) + " :No such channel");
					return;
				}
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals(":.kick")) break;
				if(tokens.length > ++i){
					this.con.kick(receiver, tokens[i], (tokens.length > ++i ? tokens[i] : "Kicked from " + receiver) );
				}
				else
					this.con.notice(getUsername(msg), "Usage: KICK <nick> [reason]");
			}
			else if(afterPrivmsg.startsWith("quit")){
				this.con.quit("Bye! :)");
			}
			else if(afterPrivmsg.startsWith("part")){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals(":.part")) break;
				if(tokens.length > ++i)
					this.con.part(tokens[i], "See you!");
				else
					this.con.notice(getUsername(msg), "Usage: PART <channel>");
			}
			else if(afterPrivmsg.startsWith("join")){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals(":.join")) break;
				if(tokens.length > ++i)
					this.con.join(tokens[i]);
				else
					this.con.notice(getUsername(msg), "Usage: JOIN <channel>");
			}
			else if(afterPrivmsg.startsWith("list")){
				String message = "I am in: ";
				for(String channel : this.con.channels){
					message += channel + " ";
				}
				this.con.msg(receiver, message);
			}
			else if(afterPrivmsg.startsWith("away")){
				this.con.away("I'm away.");
			}
			else if(afterPrivmsg.startsWith("back")){
				this.con.back();
			}
			else if(afterPrivmsg.startsWith("nick") && !this.con.isAway()){
				String[] tokens = msg.split(" ");
				int i;
				for(i = 0; i < tokens.length; i++) if (tokens[i].equals(":.nick")) break;
				if(tokens.length > ++i)
					this.con.nick(tokens[i]);
				else
					this.con.notice(getUsername(msg), "Usage: NICK <nickname>");
			}
		}
	}
}