# README #

### What is this repository for? ###

* Bot for #[hfe](http://www.herofighter-empire.com) IRC channel.
* Version 25042015

### How do I get set up? ###

* [Download and install JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [Add JDK to 'path' environment variable](http://docs.oracle.com/javase/7/docs/webnotes/install/windows/jdk-installation-windows.html#path)

### Compiling ###

Windows:
* Open command prompt
* Type "build compile"

### Running ###

Windows:
* Open command prompt
* Type "build run"